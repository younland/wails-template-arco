# README

![Wails and ArcoDesignVue](./wails-arco.png)


## About

A template for [Wails](https://wails.io/) with [ArcoDesignVue](https://arco.design/).


## Creating the Projekt
Create a new Application with the Wails CLI and this template:

```
wails init -n projectname -t https://gitee.com/younland/wails-template-arco
```

## Running the Application in Developer Mode
The easiest way is to use the Wails CLI: `wails dev`

This should hot refresh when making changes the Frontend and rebuild when making changes in the Go.

## Building the Application for Production
 
You can build you Application with: `wails build`


